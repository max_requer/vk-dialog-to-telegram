const telegram = new (require('node-telegram-bot-api'))
                        (require('./tokens.json').telegram, {
                            polling: true
                        }),
    request = require('request'),
    querystring = require("querystring"),
    DataBase = new (class DataBase{
        constructor(){
            this.dialogs = {
                //telegram id => vk id
                "441781199": 2000000002
            }
            this.proxy = 'https://tronwertfrort5.me:443'; //http://185.71.80.3:8888
            this.apiVersion = '5.73';
            this.latestMessageId = 0
        }
        getDialog(telegramDialogId){
            return new Promise((resolve, reject) => {
                resolve(this.dialogs[telegramDialogId])
            })
        }
        getApiVersion(){
            return new Promise((resolve, reject) => {
                resolve(this.apiVersion)
            })
        }
        getProxy(){
            return new Promise((resolve, reject) => {
                resolve(this.proxy)
            })
        }
        getLatestMessageId(){
            return new Promise((resolve, reject) => {
                resolve(this.latestMessageId)
            })
        }
        setLatestMessageId(id){
            this.latestMessageId = id;
            return new Promise((resolve, reject) => {
                resolve()
            })
        }
    }),
    vk = new (class VKInterface{
        constructor(token){
            //https://api.vk.com/method/METHOD_NAME?PARAMETERS&access_token=ACCESS_TOKEN&v=V
            this.token = token;
            this.userId = 464512215;
            this.userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) ArchLinux Chromium/64.0.3282.167 Chrome/64.0.3282.167 Safari/537.36'
            DataBase.getApiVersion().then(apiVersion => {this.v = apiVersion});
            DataBase.getProxy().then(proxy => {this.proxy = proxy});
        }
        getUnameById(id, counter = 0){
            return new Promise((resolve, reject) => {
                request.get({
                    url: `https://api.vk.com/method/users.get?user_ids=${id}&name_case=nom&access_token=${this.token}&v=${this.v}`,
                    proxy: this.proxy,
                    headers: {'User-Agent': this.userAgent}
                }, function(err, httpResponse, body){
                    if (err){
                        if (counter < 5) _this.getUnameById(id, ++counter).then(resolve, reject); else reject(new Error('Cannot establish connection for getting uname on 5th retry'))
                    } else {
                        try{
                            body = JSON.parse(body)
                        } catch(e){
                            reject(new SyntaxError('Cannot parse response as JSON:\n' + body))
                        }
                        if (body.error) reject(new Error('Error got: '+ JSON.stringify(body.error))); else {
                            resolve(`${body.response[0].first_name} ${body.response[0].last_name}`)
                        }
                    }
                })
            })
        }
        sendMessage(dialog, message, rand_id = -1, counter = 0){
            var _this = this;
            return new Promise((resolve, reject) => {
                !(1 + rand_id) ? rand_id = `${Math.random()}`.split('.').join('') * 1 : null;
                request.post({
                    url: `https://api.vk.com/method/messages.send?random_id=${rand_id}&peer_id=${dialog}&access_token=${this.token}&v=${this.v}`,
                    form: {
                        message: message
                    },
                    proxy: this.proxy,
                    headers: {'User-Agent': this.userAgent}
                }, function(err, httpResponse, body){
                    if (err){
                        if (counter < 5) _this.sendMessage(dialog, message, rand_id, ++counter).then(resolve, reject); else reject(new Error('Cannot establish connection for sending message on 5th retry'))
                    } else {
                        try{
                            body = JSON.parse(body)
                        } catch(e){
                            reject(new SyntaxError('Cannot parse response as JSON:\n' + body))
                        }
                        if (body.error) reject(new Error('Error got: '+ JSON.stringify(body.error))); else resolve(body.response)
                    }
                })
            })
        }
        onMessage(chat_id, callback){
            var _this = this;
            DataBase.getLatestMessageId().then(latestMessageId => {
                request.get({
                    url: `https://api.vk.com/method/messages.getHistory?count=200&rev=1&peer_id=${chat_id}&access_token=${this.token}&v=${this.v}`,
                    proxy: this.proxy,
                    headers: {'User-Agent': this.userAgent}
                }, function(err, httpResponse, body){
                    setTimeout(() => {
                        _this.onMessage(chat_id, callback)
                    }, 2000)
                    if (err) console.error(err); else {
                        try{
                            body = JSON.parse(body)
                        } catch(e){
                            console.error(new SyntaxError('Cannot parse response as JSON:\n' + body))
                        }
                        if (body.response){
                            body.response.items.forEach((message, i) => {
                                if (i == body.response.items.length - 1) DataBase.setLatestMessageId(message.id)
                                if (message.id > latestMessageId && message.user_id != _this.userId){
                                    _this.getUnameById(message.user_id).then(uname => {
                                        callback(`\`${uname.replace('`', "'")}\`\n${message.body}`)
                                    }).catch(err => {
                                        console.error(err)
                                    })
                                }
                            })
                        }
                    }
                })
            }).catch(err => {
                console.error(err)
            })
        }
    })(require('./tokens.json').vk);

var bindedChats = [];

telegram.onText(/.*/, msg => {
    // 'msg' is the received Message from Telegram
    // 'match' is the result of executing the regexp above on the text content
    // of the message

    // send back the matched "whatever" to the chat
    DataBase.getDialog(msg.chat.id).then(chatId => {
        vk.sendMessage(chatId, msg.text).catch(err => {
            telegram.sendMessage(msg.chat.id, `Failed because of\n\`${err.stack.replace('\n', '`\n`')}\``, {parse_mode : "Markdown"})
        })
        if(!(bindedChats.indexOf(chatId) + 1)){
            bindedChats.push(chatId);
            vk.onMessage(chatId, message => {
                telegram.sendMessage(msg.chat.id, message, {parse_mode : "Markdown"})
            })
        }
    });
})